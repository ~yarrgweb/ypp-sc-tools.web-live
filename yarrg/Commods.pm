# This is part of ypp-sc-tools, a set of third-party tools for assisting
# players of Yohoho Puzzle Pirates.
#
# Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
# are used without permission.  This program is not endorsed or
# sponsored by Three Rings.

package Commods;
use IO::File;
use IO::Pipe;
use HTTP::Request::Common ();
use POSIX;
use LWP::UserAgent;

use strict;
use warnings;

no warnings qw(exec);

BEGIN {
    use Exporter ();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
    $VERSION     = 1.00;
    @ISA         = qw(Exporter);
    @EXPORT      = qw(&parse_info_clientside &fetch_with_rsync
		      &parse_info_serverside &parse_info_serverside_ocean
		      %oceans %commods %clients %commodclasses
		      %vessels %shotname2damage
		      &parse_pctb_commodmap %pctb_commodmap @pctb_commodmap
		      &get_our_version &check_tsv_line &errsan
		      &pipethrough_prep &pipethrough_run
		      &pipethrough_run_along &pipethrough_run_finish
		      &pipethrough_run_gzip &http_useragent &version_core
		      &http_useragent_string_map
		      &cgipostform &yarrgpostform &cgi_get_caller
		      &set_ctype_utf8 $masterinfoversion
		      &source_tarball);
    %EXPORT_TAGS = ( );

    @EXPORT_OK   = qw();
}

our $masterinfoversion= 2; # version we understand.
#
# To extend the source-info.txt format:
#
#    * Commods.pm:parse_info1
#       add code to parse new version
#
#    * source-info.txt
#       add new information
#
# If new data should NOT be in master-info.txt too:
#
#    * update-master-info:process_some_info
#       check that code for converting source-info to master-info
#       removes the extra info; add code to remove it if necessary
#
#    * db-idempotent-populate
#       if database schema is extended, add code to copy data
#
# If new data DOES need to be in master-info.txt too:
#
#    * Commods.pm:$masterinfoversion
#       increment
#
#    * update-master-info:process_some_info
#       add code to convert new version to old, by removing
#       extra info conditionally depending on version

our %oceans; # eg $oceans{'Midnight'}{'Ruby'}{'Eta Island'}= $sources;
our %clients; # eg $clients{'ypp-sc-tools'}= [ qw(last-page) ];
our %vessels; # eg $vessels{'War Brig'}{Shot}='medium'
              #    $vessels{'War Brig'}{Volume}= 81000
              #    $vessels{'War Brig'}{Mass}= 54000
our %shotname2damage; # eg $shotname2damage{'medium'}= 3;
# $sources = 's[l]b';
#       's' = Special Circumstances; 'l' = local ; B = with Bleach

our %commods;
# eg $commods{'Fine black cloth'}{Srcs}= $sources;
# eg $commods{'Fine black cloth'}{Mass}= 700 [g]
# eg $commods{'Fine black cloth'}{Volume}= 1000 [ml]
# eg $commods{'Fine black cloth'}{Ordval}= 203921

our (%pctb_commodmap,@pctb_commodmap);

my %colours; # eg $colours{'c'}{'black'}= $sources
my (@rawcm, @nocm); # eg $rawcm[0]='fine rum'; $rawcm[1]='fine %c cloth'

my %colour_ordvals; # $colour_ordvals{'c'}{'green'}= '30';
our %commodclasses; # $commodclasses{'dye'}= '3';

# IMPORTANT
#  when extending the format of source-info in a non-backward
#  compatible way, be sure to update update-master-info too.

sub parse_info1 ($$$) {
    my ($mmfn,$src,$enoentok)= @_;
    my $mm= new IO::File $mmfn, 'r';
    if (!$mm) {
	return if $enoentok && $!==&ENOENT;
	die "$mmfn $!";
    }
    my @ctx= ();
    while (<$mm>) {
	next if m/^\s*\#/;
	next unless m/\S/;
	s/\s+$//;
	if (m/^\%(\w+)$/) {
	    my $colourkind= $1;
	    @ctx= (sub {
		m/^(\S[^\t@]*\S)(?:\t+\@(\d+\+?))?$/ or die "$_ ?";
		my ($colour,$order)=($1,$2);
		$colours{$colourkind}{$colour} .= $src;
		if (defined $order) {
		    $order =~ s/^(\d+)\+$/ $1  + $. * 10 /e;
		    $colour_ordvals{$colourkind}{$colour}= $order;
		}
	    });
	} elsif (m/^commods$/) {
	    @ctx= (sub { push @rawcm, lc $_; });
	} elsif (m/^nocommods$/) {
	    @ctx= (sub { push @nocm, lc $_; });
	} elsif (m/^commodclasses$/) {
	    @ctx= (sub {
		die unless m/^\*([_a-z]+)$/;
		$commodclasses{$1}= scalar keys %commodclasses;
	    });
	} elsif (m/^ocean (\w+)$/) {
	    my $ocean= $1;
	    keys %{ $oceans{$ocean} };
	    @ctx= (sub {
		$ocean or die; # ref to $ocean needed to work
		               # around a perl bug
		my $arch= $_;
		keys %{ $oceans{$ocean}{$arch} };
		$ctx[1]= sub {
		    $oceans{$ocean}{$arch}{$_} .= $src;
		};
	    });
	} elsif (m/^vessels$/) {
	    @ctx= (sub {
		return if m/^[-+|]+$/;
		m/^ \| \s* ([A-Z][a-z\ ]+[a-z]) \s*
		    \| \s* (small|medium|large) \s*
		    \| \s* ([1-9][0-9,]+) \s*
		    \| \s* ([1-9][0-9,]+) \s*
		    \| $/x
		    or die;
		my $name= $1;
		my $v= { Shot => $2, Volume => $3, Mass => $4 };
		foreach my $vm (qw(Volume Mass)) { $v->{$vm} =~ s/,//g; }
		$vessels{$name}= $v;
	    });
	} elsif (m/^shot$/) {
	    @ctx= (sub {
		m/^ ([a-z]+) \s+ (\d+) $/x or die;
		$shotname2damage{$1}= $2;
	    });
	} elsif (m/^client (\S+.*\S)$/) {
	    my $client= $1;
	    $clients{$client}= [ ];
	    @ctx= (sub {
		my $bug= $_;
		push @{ $clients{$client} }, $bug;
	    });
	} elsif (s/^ +//) {
	    my $indent= length $&;
	    die "wrong indent $indent" unless defined $ctx[$indent-1];
	    &{ $ctx[$indent-1] }();
	} else {
	    die "bad syntax";
	}
    }
    $mm->error and die $!;
    close $mm or die $!;

#print Dumper(\%oceans);
#print Dumper(\@rawcm);
	
    %commods= ();
    my $ca;
    my $lnoix=0;
    $ca= sub {
	my ($s,$ss,$ordbase) = @_;
#print STDERR "ca($s,,".(defined $ordbase ? $ordbase : '?').")\n";
	if ($s !~ m/\%(\w+)/) {
	    my ($name, $props) = $s =~
		/^(\S[^\t]*\S)(?:\t+(\S.*\S))?$/
		or die "bad commodspec $s";
	    return if grep { $name eq $_ } @nocm;
	    my $ucname= ucfirst $name;
	    $commods{$ucname}{Srcs} .= $ss;
	    my $c= $commods{$ucname};
	    $c->{Volume}= 1000;
	    $c->{Flags}= '';
	    my ($ordval, $ordclassval);
	    foreach my $prop (defined $props ? split /\s+/, $props : ()) {
		if ($prop =~ m/^([1-9]\d*)(k?)g$/) {
		    $c->{Mass}= $1 * ($2 ? 1000 : 1);
		} elsif ($prop =~ m/^([1-9]\d*)l$/) {
		    $c->{Volume}= $1 * 1000;
		} elsif ($prop =~ m/^\*([_a-z]+)$/) {
		    $c->{Class}= $1;
		    die "$1" unless exists $commodclasses{$1};
		    $ordclassval= 1e7 + $commodclasses{$1} * 1e7;
		} elsif ($prop =~ m/^\@(\d+\+?)$/) {
		    $ordval= $1;
		    $ordval =~ s/^(\d+)\+$/ $1 + $lnoix * 10 /e;
		} elsif ($prop =~ m/^\!([a-z]+)$/) {
		    $c->{Flags} .= $1;
		} else {
		    die "unknown property $prop for $ucname";
		}
	    }
	    $c->{ClassOrdval}= $ordclassval;
	    if (defined $ordbase && defined $ordval && defined $ordclassval) {
		my $ordvalout= $ordbase + $ordval + $ordclassval;
		$c->{Ordval}= $ordvalout;
#print STDERR "ordval $ordvalout $name OV=$ordval OB=$ordbase OCV=$ordclassval\n";
	    } else {
#print STDERR "ordval NONE $name\n";
            }
	    return;
	}
	die "unknown $&" unless defined $colours{$1};
	my ($lhs,$pctlet,$rhs)= ($`,$1,$');
	foreach my $c (keys %{ $colours{$pctlet} }) {
	    my $ordcolour= $colour_ordvals{$pctlet}{$c};
	    &$ca($lhs.$c.$rhs,
		 $ss .'%'. $colours{$pctlet}{$c},
		 defined($ordbase) && defined($ordcolour)
		     ? $ordbase+$ordcolour : undef);
	}
    };
    foreach (@rawcm) { $lnoix++; &$ca($_,$src,0); }
}

sub parse_info_clientside () {
    my $master= fetch_with_rsync("info-v$masterinfoversion");
    parse_info1($master,'s',1);
    parse_info1('_local-info.txt','s',1);
}

sub fetch_with_rsync ($) {
    my ($stem) = @_;

    my $rsync= $ENV{'YPPSC_YARRG_RSYNC'};
    $rsync= 'rsync' if !defined $rsync;

    my $local= "_master-$stem.txt";
    my $src= $ENV{'YPPSC_YARRG_DICT_UPDATE'};
    if ($src) {
	my $remote= "$src/master-$stem.txt";
	$!=0; system 'rsync','-Lt','--',$remote,$local;
	die "$? $!" if $! or $?;
    }
    return $local;
}

sub parse_info_serverside () {
    parse_info1('source-info.txt','s',0);
    parse_info1('tree-info.txt','t',1);
}
sub parse_info_serverside_ocean ($) {
    my ($oceanname) = @_;
    die "unknown ocean $oceanname ?" unless exists $oceans{$oceanname};
    parse_info1("_ocean-".(lc $oceanname).".txt", 's',0);
}

sub parse_pctb_commodmap () {
    undef %pctb_commodmap;
    foreach my $commod (keys %commods) { $commods{$commod}{Srcs} =~ s/b//; }

    my $c= new IO::File '_commodmap.tsv';
    if (!$c) { $!==&ENOENT or die $!; return 0; }

    while (<$c>) {
	m/^(\S.*\S)\t(\d+)\n$/ or die "$_";
	die if defined $pctb_commodmap{$1};  $pctb_commodmap{$1}= $2;
	die if defined $pctb_commodmap[$2];  $pctb_commodmap[$2]= $1;
	$commods{$1}{Srcs} .= 'b';
    }
    $c->error and die $!;
    close $c or die $!;
    return 1;
}

sub get_our_version ($$) {
    my ($aref,$prefix) = @_;
    $aref->{"${prefix}name"}= 'ypp-sc-tools yarrg';
    $aref->{"${prefix}fixes"}= 'lastpage checkpager';
    $aref->{"${prefix}version"}= version_core();
    return $aref;
    # clientname	"ypp-sc-tools"
    # clientversion	2.1-g2e06a26  [from git-describe --tags HEAD]
    # clientfixes	"lastpage"  [space separated list]
}

sub version_core () {
    my $version= `
	if type -p git-describe >/dev/null 2>&1; then
		gd=git-describe
	else
		gd="git describe"
	fi
	\$gd --tags HEAD || echo 0unknown
    `; $? and die $?;
    chomp($version);
    return $version;
}

sub pipethrough_prep () {
    my $tf= IO::File::new_tmpfile() or die $!;
    return $tf;
}

sub pipethrough_run_along ($$$@) {
    my ($tf, $childprep, $cmd, @a) = @_;
    $tf->error and die $!;
    $tf->flush or die $!;
    $tf->seek(0,0) or die $!;
    my $fh= new IO::File;
    my $child= $fh->open("-|"); defined $child or die $!;
    if (!$child) {
	open STDIN, "<&", $tf;
	&$childprep() if defined $childprep;
	exec $cmd @a; die "@a $!";
    }
    return $fh;
}
sub pipethrough_run_finish ($$) {
    my ($fh, $what)= @_;
    $fh->error and die $!;
    close $fh or die "$what $! $?";  die $? if $?;
}

sub pipethrough_run ($$$@) {
    my ($tf, $childprep, $cmd, @a) = @_;
    my $pt= pipethrough_run_along($tf,$childprep,$cmd,@a);
    my $r;
    { undef $/; $!=0; $r= <$pt>; }
    defined $r or die $!;
    pipethrough_run_finish($pt, "@a");
    return $r;
}
sub pipethrough_run_gzip ($) {
    pipethrough_run($_[0],undef,'gzip','gzip');
}

sub yarrgpostform ($$) {
    my ($ua, $form) = @_;
    my $dest= $ENV{'YPPSC_YARRG_YARRG'};
    get_our_version($form, 'client');
    die unless $dest =~ m,/$,;
    return cgipostform($ua, "${dest}commod-update-receiver", $form);
}    

sub cgipostform ($$$) {
    my ($ua, $url, $form) = @_;
    my $req= HTTP::Request::Common::POST($url,
					 Content => $form,
					 Content_Type => 'form-data');
    if ($url =~ m,^\.?/,) {
	my $tf= pipethrough_prep();
	print $tf $req->content() or die $!;
#print STDERR "[[[",$req->content(),"]]]";
	my $out= pipethrough_run($tf, sub {
	    $ENV{'REQUEST_METHOD'}= 'POST';
	    $ENV{'QUERY_STRING'}= '';
	    $ENV{'PATH_TRANSLATED'}= $url;
	    $ENV{'PATH_INFO'}= '';
	    $ENV{'HTTP_HOST'}= 'localhost';
	    $ENV{'REMOTE_ADDR'}= '127.0.0.1';
	    $ENV{'GATEWAY_INTERFACE'}= 'CGI/1.1';
	    $ENV{'DOCUMENT_ROOT'}= '.';
	    $ENV{'SCRIPT_FILENAME'}= $url;
	    $ENV{'SCRIPT_NAME'}= $url;
	    $ENV{'HTTP_USER_AGENT'}= 'Commods.pm local test';

	    foreach my $f (qw(Content_Length Content_Type)) {
		$ENV{uc $f}= $req->header($f);
	    }
#system 'printenv >&2';
	}, "$url", "$url");
	$out =~ s/\r\n/\n/g;
	$out =~ m,^Content-Type: text/plain.*\n\n, or die "$out ?";
	return $';
    } else {
	my $resp= $ua->request($req);
	die $resp->status_line."\n".$resp->content."\n "
	    unless $resp->is_success;
	return $resp->content();
    }
}

our %check_tsv_done;

sub errsan ($) {
    my ($value) = @_;
    $value =~ s/[^-+\'. A-Za-z0-9]/ sprintf "\\x%02x",ord $& /ge;
    return "\"$value\"";
}

sub check_tsv_line ($$) {
    my ($l, $bad_data_callback) = @_;
    my $bad_data= sub { &$bad_data_callback("bad data: line $.: $_[0]"); };
    
    chomp($l) or &$bad_data('missing end-of-line');

    $l =~ m/[\t [:graph:]]/ or &$bad_data('nonprinting char(s) '.sprintf "%#x", ord $&);
    my @v= split /\t/, $l, -1;
    @v==6 or &$bad_data('wrong number of fields');
    $v[0] = ucfirst lc $v[0]; # YPP anomalously has "Rum Spice"
    $v[1] =~ s/^\s+//; $v[1] =~ s/\s+$//; # ooo don't check :-(
    my ($commod,$stall) = @v;

    !keys %commods or
	defined $commods{$commod} or
	&$bad_data("unknown commodity ".errsan($commod));
    
    $stall =~ m/\p{IsAlnum}/ or
	&$bad_data("stall does not contain with alphanumeric".errsan($stall));
    !exists $check_tsv_done{$commod,$stall} or
	&$bad_data("repeated data ".errsan($commod).",".errsan($stall));
    $check_tsv_done{$commod,$stall}= 1;
    foreach my $i (2..5) {
	my $f= $v[$i];
	$f =~ m/^(|0|[1-9][0-9]{0,5}|\>1000)$/ or
	    &$bad_data("bad field $i ".errsan($f));
	($i % 2) or ($f !~ m/\>/) or
	    &$bad_data("> in field $i price ".errsan($f));
    }

    foreach my $i (2,4) {
	&$bad_data("price with no qty or vice versa (field $i)")
	    if length($v[$i]) xor length($v[$i+1]);
    }
    length($v[2]) or length($v[4]) or
	&$bad_data("commodity entry with no buy or sell offer");
    
    return @v;
}

sub cgi_get_caller () {
    my $caller= $ENV{'REMOTE_ADDR'};
    $caller= 'LOCAL' unless defined $caller;

    my $fwdf= $ENV{'HTTP_X_FORWARDED_FOR'};
    if (defined $fwdf) {
	$fwdf =~ s/\s//g;
	$fwdf =~ s/[^0-9.,]/?/g;
	$caller= "$fwdf";
    }
    return $caller;
}

sub set_ctype_utf8 () {
    setlocale(LC_CTYPE, "en.UTF-8");
}

sub http_useragent_string_map ($$) {
    my ($caller_lib_agent, $reason_style_or_caller) = @_;
    $caller_lib_agent =~ y/A-Za-z/N-ZA-Mn-za-m/;
    $caller_lib_agent =~ s/\s/_/g;
    my $version= version_core();
    return "yarrg/$version ($reason_style_or_caller)".
	   " $caller_lib_agent".
	   " (http://yarrg.chiark.net/intro)";
}

sub http_useragent ($) {
    my ($who) = @_;
    my $ua= LWP::UserAgent->new;
    my $base= $ua->_agent();
    $ua->agent(http_useragent_string_map($base, $who));
    return $ua;
}

sub source_tarball ($$) {
    my ($sourcebasedir,$spitoutfn) = @_;

    my $pipe= new IO::Pipe or die $!;
    my $pid= fork();  defined $pid or die $!;
    if (!$pid) {
	$ENV{'YPPSC_YARRG_SRCBASE'}= $sourcebasedir;
	$pipe->writer();
	exec '/bin/sh','-c','
		cd -P "$YPPSC_YARRG_SRCBASE"
		(
		 git-ls-files -z;
		 git-ls-files -z --others --exclude-from=.gitignore;
		 if test -d .git; then find .git -print0; fi
		) | (
		 cpio -Hustar -o --quiet -0 -R 1000:1000 || \
		 cpio -Hustar -o --quiet -0
		) | gzip
	';
	die $!;
    }
    $pipe->reader();

    my ($d, $l);
    while ($l= read $pipe, $d, 65536) {
	$spitoutfn->($d);
    }
    waitpid $pid,0;
    defined $l or die "read pipe $!";
    $pipe->error and die "pipe error $!";
    close $pipe;
    # deliberately ignore errors
}

1;
