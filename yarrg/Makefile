# This is part of ypp-sc-tools, a set of third-party tools for assisting
# players of Yohoho Puzzle Pirates.
#
# Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The parts of the code used for the website (including the web/
# directory and the rs*.[ch] which make up the routesearch program)
# are released instead under the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
# are used without permission.  This program is not endorsed or
# sponsored by Three Rings.


CC= gcc
OPTIMISE= -O2
WERROR= -Werror
WARNINGS= -Wall -Wwrite-strings -Wpointer-arith -Wmissing-prototypes \
	  -Wstrict-prototypes -Wno-format-zero-length
DEBUG=-g

CFLAGS += $(WARNINGS) $(WERROR) $(OPTIMISE) $(DEBUG)

TARGETS_CLIENT= yarrg
TARGETS_SERVER= routesearch
TARGETS= $(TARGETS_CLIENT) $(TARGETS_SERVER)

default: clean-other-directory client
client: $(TARGETS_CLIENT)
server: $(TARGETS_SERVER)
all: client server

CONVERT_OBJS= convert.o ocr.o pages.o structure.o rgbimage.o resolve.o
COMMON_OBJS= common.o
ROUTESEARCH_OBJS= rsvalue.o rsmain.o rssql.o rssearch.o

yarrg:	$(CONVERT_OBJS) $(COMMON_OBJS) -lnetpbm -lXtst -lX11 -lpcre -lm
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(CONVERT_OBJS): common.h ocr.h convert.h structure.h

routesearch:	$(ROUTESEARCH_OBJS) $(COMMON_OBJS) -lsqlite3 -lglpk -lm
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

$(ROUTESEARCH_OBJS): common.h rscommon.h
$(COMMON_OBJS): common.h

clean:
	rm -f *.o core core.* *~ vgcore.*
	rm -f t t.* u u.* v v.* *.tmp *.orig *.rej
	rm -f ypp-commodities

realclean: clean
	rm -f $(TARGETS)
	rm -f _pages.ppm _pages.ppm.gz _upload-*.html _commodmap.tsv
	rm -f _master-*.txt _master-*.txt.gz _local-*.txt
	rm -f ./#pages#.ppm ./#upload-*#.html ./#commodmap#.tsv
	rm -f ./#master-*#.txt ./#local-*#.txt raw.tsv

clean-other-directory:
	@set -e; if test -d ../pctb && ! test -L ../pctb; then \
		echo '*** tidying up ../pctb; moving local data here ***'; \
		set -x; \
		find ../pctb -path '../pctb/_local-*.txt' -exec mv '{}' . \;; \
		$(MAKE) -C ../pctb -f ../yarrg/Makefile realclean; \
		rmdir ../pctb; \
	fi
