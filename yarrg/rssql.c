/*
 * Route searcher - database helper functions
 */
/*
 *  This is part of the YARRG website, a tool for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "rscommon.h"

sqlite3 *db;
sqlite3_stmt *ss_ipair;

int islandtablesz;

DEBUG_DEFINE_DEBUGF(sql);
DEBUG_DEFINE_SOME_DEBUGF(sql,debug2f);

static int busy_handler(void *u, int previous) {
  debugf("[[DB BUSY %d]]",previous);
  sysassert(! usleep(5000) );
  return 1;
}

void setup_sql(const char *database) {
  sqlite3_stmt *sst;
  
  SQL_MUST( sqlite3_open(database, &db) );
  SQL_MUST( sqlite3_busy_handler(db, busy_handler, 0) );

  sst= sql_prepare("BEGIN","(begin)");
  assert( !SQL_STEP(sst) );
  sqlite3_finalize(sst);

  islandtablesz= 1 + sql_single_int("SELECT max(islandid) FROM islands");
  debugf("SQL islandtablesz=%d\n",islandtablesz);
}

int sql_single_int(const char *stmt) {
  sqlite3_stmt *sst;
  sst= sql_prepare(stmt,"(single int)");
  assert( SQL_STEP(sst) );
  int rv= sqlite3_column_int(sst,0);
  sqlite3_finalize(sst);
  return rv;
}

void sql_fatal(const char *stmt_what, int sqr, const char *act_what) {
  fatal("SQL call failed, stmt %s code %d: %s: %s",
	stmt_what, sqr, sqlite3_errmsg(db), act_what);
}

void sql_bind(sqlite3_stmt *ss, int index, int value,
	      const char *ss_what, const char *val_what) {
  debug2f("SQL BIND %s #%d = %d = %s\n", ss_what, index, value, val_what);
  int sqr= sqlite3_bind_int(ss, index, value);
  if (sqr) sql_fatal(ss_what, sqr,
		     masprintf("bind #%d (%s)", index, val_what));
}
  
sqlite3_stmt *sql_prepare(const char *stmt, const char *what) {
  sqlite3_stmt *ssr;
  debugf("SQL PREPARE %s [[\n%s\n]]\n", what, stmt);
  SQL_MUST( sqlite3_prepare(db, stmt, -1, &ssr, 0) );
  return ssr;
}

int sql_step_distinct(sqlite3_stmt *ssh, const char *ssh_string,
		      const char *file, int line,
		      int *cols, int ncols, int nkeycols) {
  for (;;) {
    if (!sql_step(ssh, ssh_string, file, line)) return 0;

    int i;
    for (i=0; i<ncols; i++) {
      int v= sqlite3_column_int(ssh, i);
      if (v == cols[i]) continue;
      
      assert(i<nkeycols);
      cols[i++]= v;
      for ( ; i<ncols; i++)
	cols[i]= sqlite3_column_int(ssh, i);
      return 1;
    }
  }
}

int sql_step(sqlite3_stmt *ssh, const char *ssh_string,
	     const char *file, int line) {
  for (;;) {
    int sqr;
    sqr= sqlite3_step((ssh));
    switch (sqr) {
    case SQLITE_DONE:
      debug2f("SQL %s DONE\n",ssh_string);
      return 0;
    case SQLITE_ROW:
      if (DEBUGP(sql2)) {
	int i;
	fprintf(debug,"SQL %s R",ssh_string);
	for (i=0; i<sqlite3_column_count(ssh); i++) {
	  fputc('\t',debug);
	  const char *txt= (const char*)sqlite3_column_text(ssh,i);
	  fputs(txt ? txt : "<null>", debug);
	}
	fputs("\n",debug);
      }
      return 1;
    default: fatal("SQL step failed at %s:%d: code %d: %s: %s",
		   file, line, sqr, sqlite3_errmsg(db), ssh_string);
    }
  }
}
