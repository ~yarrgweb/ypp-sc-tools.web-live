<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates form contents for selecting a list
 of locations (eg, a route).


</%doc>
<%args>
$qa
$dbh
$emsg_r
$warningfs_r

$enterwhat
$islandids_r
$archipelagoes_r
</%args>

%#---------- textbox, user enters route as string ----------
% if (!$qa->{Dropdowns}) {

<% $enterwhat %>
% if (defined($archipelagoes_r)) {
(islands, or archipelagoes,
% } else {
(islands
% }
separated by |s or commas; abbreviations are OK):<br>

<& qtextstring, qa => $qa, dbh => $dbh, emsgstore => $emsg_r,
    thingstring => defined($archipelagoes_r) ? 'routestring' : 'islandstring',
    prefix => 'rl', boxopts => 'size=80',
    onresults => sub {
	foreach (@_) {
	my ($canonname, $island, $arch) = @$_;
		push @$islandids_r, $island;
		push @$archipelagoes_r, defined $island ? undef : $arch
			if defined $archipelagoes_r;
	}
    }
 &>

% } else { #---------- dropdowns, user selects from menus ----------

<%perl>
my %islandid2;
my ($sth,$row);
my @archlistdata;
my %islandlistdata;
$islandlistdata{'none'}= [ [ "none", "Select island..." ] ];

my $optionlistmap= sub {
	my ($optlist, $selected) = @_;
	my $out='';
	foreach my $entry (@$optlist) {
		$out.= sprintf('<option value="%s" %s>%s</option>',
			encode_entities($entry->[0]),
			defined $selected && $entry->[0] eq $selected
				? 'selected' : '',
			encode_entities($entry->[1]));
	}
	return $out;
};

$sth= $dbh->prepare("SELECT DISTINCT archipelago FROM islands
			    ORDER BY archipelago;");
$sth->execute();

while ($row=$sth->fetchrow_arrayref) {
	my ($arch)= @$row;
	push @archlistdata, [ $arch, $arch ];
	$islandlistdata{$arch}= [ [ "none", "Whole arch" ] ];
}

$sth= $dbh->prepare("SELECT islandid,islandname,archipelago
                            FROM islands
			    ORDER BY islandname;");
$sth->execute();

while ($row=$sth->fetchrow_arrayref) {
	my $arch= $row->[2];
	push @{ $islandlistdata{'none'} }, [ @$row ];
	push @{ $islandlistdata{$arch} }, [ @$row ];
	$islandid2{$row->[0]}= { Name => $row->[1], Arch => $arch };
}

my %resetislandlistdata;
foreach my $arch (keys %islandlistdata) {
	$resetislandlistdata{$arch}=
		$optionlistmap->($islandlistdata{$arch}, '');
}

</%perl>

<&| script &>
ms_lists= <% to_json_protecttags(\%resetislandlistdata) %>;
function ms_Setarch(dd) {
  debug('ms_SetArch '+dd+' arch='+arch);
  var arch= document.getElementsByName('archipelago'+dd).item(0).value;
  var got= ms_lists[arch];
  if (got == undefined) return; // unknown arch ?  hrm
  debug('ms_SetArch '+dd+' arch='+arch+' got ok');
  var select= document.getElementsByName('islandid'+dd).item(0);
  select.innerHTML= got;
  debug('ms_SetArch '+dd+' arch='+arch+' innerHTML set');
}
</&script>

<table style="table-layout:fixed; width:90%;">

<tr>
%	for my $dd (0..$qa->{Dropdowns}-1) {
<td>
<select name="archipelago<% $dd %>" onchange="ms_Setarch(<% $dd %>)">
<option value="none">Whole ocean</option>
<% $optionlistmap->(\@archlistdata, $qa->{"archipelago$dd"}) %></select></td>
%	}
</tr>

<tr>
%	for my $dd (0..$qa->{Dropdowns}-1) {
%		my $arch= $qa->{"archipelago$dd"};
%		$arch= 'none' if !defined $arch;
<td>
<select name="islandid<% $dd %>">
<% $optionlistmap->($islandlistdata{$arch}, $qa->{"islandid$dd"}) %>
</select></td>
%	}
</tr>

</table>

<%perl>

my $argorundef= sub {
	my ($dd,$base) = @_;
	my $thing= $qa->{"${base}${dd}"};
	$thing= undef if defined $thing and $thing eq 'none';
	return $thing;
};

for my $dd (0..$qa->{Dropdowns}-1) {
	my $arch= $argorundef->($dd,'archipelago');
	my $island= $argorundef->($dd,'islandid');
	next unless defined $arch or defined $island;
	if (defined $island and defined $arch) {
		my $ii= $islandid2{$island};
		my $iarch= $ii->{Arch};
		if ($iarch ne $arch) {
			push @$warningfs_r, sub {
</%perl>
 Specified archipelago <% $arch %> but
 island <% $ii->{Name} %>
 which is in <% $iarch %>; using the island.<p>
<%perl>
			};
		}
		$arch= undef;
	}
	push @$archipelagoes_r, $arch;
	push @$islandids_r, $island;
}

</%perl>
<p>

% }
