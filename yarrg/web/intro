<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby
 Copyright (C) 2009 Naath Cousins

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates the introduction.


</%doc>
<& docshead:head &>
<style type="text/css">
<& style.css &>
  body { margin: 0; color: #000000; background: #c5c7ae; }
  div.emph {
     margin-left: 1em; padding-left: 1em; padding-top: 0.01em;
     margin-right: 1em; padding-right: 1em; padding-bottom: 0.05em;
     background: #ffffff;
  }
</style>
<div class="navoptbar">
<& navbar &>
</div>
<div class="docs">

<h1>Guide to YARRG</h1>

YARRG (Yet Another Revenue Research Gatherer) is a third-party tool
for helping find profitable trades and trade routes in Yohoho Puzzle
Pirates.  It was inspired by
<a href="http://pctb.crabdance.com/">PCTB</a>.

<p>

Information about commodity prices is collected by pirates like you
and uploaded to the YARRG server, using the special yarrg upload
client.  You can then query and search on this website for commodity
prices and good trade routes.


<p>

<div class="emph">
<h2>Quick start for experts</h2>

<h3>Straight to the commodity prices database</h3>

If you already know about trading on Puzzle Pirates, understand what
the YARRG website is for, and just want to get down to playing with
it right away, simply select your ocean:

<p>
<div align="center">
% my @oceans= ocean_list();
% my $delim= '';
% foreach my $ocean (@oceans) {
%	my $uri= URI->new('lookup');
%	$uri->query_form('ocean', $ocean) if $ocean ne 'Midnight';
<% $delim %><a href="<% $uri->path_query() |h %>"><% $ocean |h %></a>
%	$delim=' | ';
% }
</div>

<h3>But before you start ...</h3>

If you are going to use YARRG seriously you will almost certainly want
to upload your own data.  See <a href="upload">Uploading to YARRG</a>
for details.

<p>

You will also probably at some point want to read the
<a href="docs">Documentation for Experts</a> which covers some
features of the website which you might not guess just from using it.

</div>

<h2>How to use the YARRG website</h2>

YARRG is designed to help pirates make profitable trades.  It is not
designed as a trading tutorial and some familiarity with trading in
YPP is assumed. <a
href="http://yppedia.puzzlepirates.com/Trading">yppedia.puzzlepirates.com/Trading</a>
contains some advice on trading basics.

<h3>So how do I use it then?</h3> 

<p> First look at the top of the page.  You need to select your Ocean,
the sort of Interface you would like and the type of Query you wish to
ask.

<p> 

<h3>Selecting your Ocean</h3> 

<p> 

Which ocean do you want to trade on?
Just click on that ocean's name.

<p> 

<h3>Selecting your Interface</h3>

<p> You can choose to enter commodity and island names by typing them
into a text entry field or by selecting them from a drop down menu.
The "select from menu" option allows only a restricted number of
options to be entered into each field.

<p>

<h3>Type of Query</h3> 

<p> 

There are currently four types of query available which answer
slightly different questions.

<p> 

<h4>Trades for route</h4>

<p> 

<b>For pirates who know where they are going.</b>  Trades for route allows
you to enter a route that you are planning to sail and find out if
there are any profitable trades you can make along the way.  This is
for pirates who already have a definite sailing plan for some reason.
Under "Enter Route" you need to type (or select from menus) the names
of the islands or Archipelagoes that you plan to visit.  If you wish
to know about arbitrage opportunities at an island you can enter just
that island name.  If you enter only one Archipelago then you will get
arbitrage opportunities in that archipelago; if you enter the same
archipelago twice you'll get trades within that archipelago.

<p> 

<h4>Prices for commodity</h4> 

<p> 

<b>For pirates wanting to sell or buy a specific commodity</b> prices for
commodity returns the best buy and sell prices for the selected
commodity at each island where trades are available.  Type (or select
from a menu) the name of one commodity at a time.  This is useful for
pirates wishing to purchase or sell a specific commodity and wanting
the best price.  

<p> 

<h4>Offers at location</h4>

<p>

<b>For pirates wanting more details about commodity prices at specified
locations.</b> Offers at location returns all the offers to buy and sell
wood at the selected location(s). Type (or select) a commodity and the
islands (or achipelagos) you are interested in.  This is useful when
the best buy or sell offer is for only a small quantity: you can check
what the others offers are like.

<p>

<h4>Find Profitable route</h4> 

<p>

<b>For pirates just wanting to make some poe moving commodities about.</b> Find
Profitable Route finds you a profitable trading route: first select
either "open-ended" (which might take you anywhere) or "circular"
(which takes you back to where you started) then type (or select) your
starting point (or points - you may for instance have two ships in
different locations either of which you could use).

<p>

<h3>Data age</h3>

<p>

Data age tells you how long ago data was uploaded from each island in
your ocean.  Data changes very quickly and trusting old data to be
accurate is unlikely to be rewarding.

<p>

<h3>What do the results mean</h3>

<p>

"Prices for commodity" and "Offers at location" give very similar results;
there will be two tables: the first titled "Offers to BUY" your
commodity, the second "Offers to SELL" it.  The tables can be sorted
as you wish by clicking on the arrows in the headings.

<p>

"Trades for route" and "Find profitable route" both return voyage plans.
"Find profitable route" has an additional step to get at your results -
when you enter your query you are given a list of 15 good routes for
total profit and 15 good routes for profit per league which visit
different islands. Select your preferred route: perhaps you will choose
to take the best profit or perhaps you need to miss out one island
because it is blockaded or another because you don't have the charts
to get to or from it.  If you have asked for a trade for route or
after selecting the profitable route you prefer you will get a Voyage
trading plan - this provides you with instructions for what to do (buy
this, sell that, sail there).

<p>

Sometimes no useful trades are available.  In this case the results
section says simply "No profitable trading opportunities were found".

<p>

After this there is a table reporting the age of the data used.  And
then a table of "Relevant trades" which lists just about everything you
could want to know about each trade the plan suggests you make (yes,
this table is fairly complicated).  The ticky boxes down the side of
this table allow you to choose which trades you like the look of -
untick any that you are not interested in (for instance you may be
uninterested in trades with a low profit margin).

<p>

<h3>What are the Advanced options</h3>

<p>

These options are very useful, and not very advanced, but not needed
for searching the data.

<p>

<h4>Capacity</h4>

<p>

You can't trade more than fits in your hold.  If you don't say
anything YARRG assumes that you have infinite hold space (perhaps you
will go over the route many times).  You can enter a ship-type, a mass
or a volume; additionally you can subtract quantities of commodities
from a ship-type (for instance 'sloop - 10 grog' gives the capacity of
a sloop that has 10 grog already on board); click the ? by this field
to get a full description of permitted entries here.

<p>

<h4>Capital</h4>

<p>
 
How much poe do you have?  If you don't say anything YARRG assumes you
have infinite poe to spend, but you probably don't.  Enter the amount
you are willing to spend on commodities for trading with.

<p>

<h4>Expected losses</h4> 

<p>

How much do you expect to lose to the dastardly brigands?  Enter either
a % (as eg - 1%) or a fraction (as eg - 1/100) of the goods you
expect to loose to Brigands.  Brigands take 10% if they defeat you,
but only you know how often you are defeated.  Note that this doesn't
include rum-consumed so you will need to account for that
seperately.

<p>

<h4>Max distance</h4>

<p>

How far are you willing to sail today?  The default is 20 leagues, the
maximum supported is 35.  Are you willing to sail all the way across
the ocean for a profit?  Or would you prefer to go only a short way?

<p>

<h3>Other fairly useful information</h3>

<p>

The search information is encoded in the URL - you can bookmark pages
for searches you want to perform a lot and go straight to them without
re-entering you data (or you can copy the URL and tell your hearties
and crew mates if you want to).  There's no need to come back to the
introductory page each time.

<p>

Javascript magic: when you type things into the text entry fields
YARRG will try to guess what you meant.  If you pause for a moment then
beneath the text entry field you'll see what YARRG thinks you meant.
If it's got it correct you don't have to type any more of that
commodity/island name: just go on to the next one.

<p>

You'll probably want to upload data to ensure you have fresh data
available.  Unfortunately YARRG only has a Linux upload client at
present (a windows one is in the works).  You should <a
href="upload">read about how to upload</a>: particularly the linked
README page for information about the client which contain
easy-to-follow instructions to download, install and run it in the
form of a few comand line instructions that you can copy and paste.

<p>

It's all free, both free-as-in-beer or free-as-in-speech.  If you're
interested, read the <a href="devel">Development pages</a> for more
info about how to contribute or how to run your own website using
YARRG data (perhaps you don't like our colour choices)



<h2>Contacting the YARRG developers</h2>

Email Ian Jackson ijackson (at) chiark.greenend.org.uk.  Or talk to
any Fleet Officer or above of the crew Special Circumstances on the
Cerulean Ocean.
<p>

</div>
<& footer &>
