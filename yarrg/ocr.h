/*
 * ocr.c forms a mostly-self-contained bit
 * so we put its declarations in this separate file
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#ifndef OCR_H
#define OCR_H

#include "common.h"


#define AADEPTH 2
#define AAMAXVAL ((1<<AADEPTH)-1)
 /* Change this ?  Also change in
  *   dictionary-manager            set aadepth
  *   dictionary-update-receiver    my $aadepth
  */


typedef uint32_t Pixcolv;
#define PIXCOL_P_PER_WORD (32 / AADEPTH)

#define OCR_MAX_H    30
#define PIXCOL_WORDS ((OCR_MAX_H + PIXCOL_P_PER_WORD - 1) / PIXCOL_P_PER_WORD)

typedef struct { Pixcolv w[PIXCOL_WORDS]; } Pixcol;

static inline int pixcol_p_word(int y) {
  return y / PIXCOL_P_PER_WORD;
}
static inline int pixcol_p_shift(int y) {
  return (y % PIXCOL_P_PER_WORD)*AADEPTH;
}
static inline void pixcol_p_add(Pixcol *pixcol, int y, unsigned pixval) {
  pixcol->w[pixcol_p_word(y)] |= pixval << pixcol_p_shift(y);
}
static inline unsigned int pixcol_p_get(const Pixcol *pixcol, int y) {
  return (pixcol->w[pixcol_p_word(y)] >> pixcol_p_shift(y)) & AAMAXVAL;
}
static inline int pixcol_cmp(const Pixcol *pixcol1, const Pixcol *pixcol2) {
  return memcmp(pixcol1, pixcol2, sizeof(*pixcol1));
}
static inline int pixcol_nonzero(const Pixcol *pixcol) {
  static const Pixcol zero;
  return pixcol_cmp(pixcol, &zero);
}

#if AADEPTH==3
# define PRPIXCOL1 "%0*" PRIo32
# define PIXCOL_P_PER_FMT 1
#elif AADEPTH==2
# define PRPIXCOL1 "%0*" PRIx32
# define PIXCOL_P_PER_FMT 2
#else
# error need to implement PRPIXCOL1 for this AADEPTH
#endif

# define PIXCOL_FMT_PER_WORD (PIXCOL_P_PER_WORD / PIXCOL_P_PER_FMT)

#if PIXCOL_WORDS==2
# define PIXCOL_PRFMT				\
			      PRPIXCOL1 "-"	\
			      PRPIXCOL1
# define PIXCOL_PRVAL(pixcol)						\
			      PIXCOL_FMT_PER_WORD, (pixcol).w[1],	\
			      PIXCOL_FMT_PER_WORD, (pixcol).w[0]
#elif PIXCOL_WORDS==3
# define PIXCOL_PRFMT				\
			      PRPIXCOL1 "-"	\
			      PRPIXCOL1 "-"	\
			      PRPIXCOL1
# define PIXCOL_PRVAL(pixcol)						\
			      PIXCOL_FMT_PER_WORD, (pixcol).w[2],	\
			      PIXCOL_FMT_PER_WORD, (pixcol).w[1],	\
			      PIXCOL_FMT_PER_WORD, (pixcol).w[0]
#else
# error need to implement PIXCOL_PR{FMT,VAL} for this PIXCOL_WORDS
#endif

typedef struct {
  const char *s; /* valid until next call to ocr() */
  int l,r; /* column numbers */
  int match; /* match context index */
  unsigned ctxmap; /* possible match contexts */
} OcrResultGlyph;


typedef const struct OcrCellTypeInfo *OcrCellType;
extern const struct OcrCellTypeInfo ocr_celltype_text;
extern const struct OcrCellTypeInfo ocr_celltype_number;
const char *ocr_celltype_name(OcrCellType ct);


typedef struct OcrReader OcrReader;
OcrReader *ocr_init(int h);
void ocr_showcharsets(void);

OcrResultGlyph *ocr(OcrReader *rd, OcrCellType, int w, const Pixcol cols[]);
  /* return value is array terminated by {0,-1,-1}
   * array is valid until next call to ocr()
   */


extern const char *o_resolver;

FILE *resolve_start(void);
void resolve_finish(void);


#endif /*OCR_H*/
